#!/usr/bin/env bash

fabmanager create-admin --app superset
superset db upgrade
superset load_examples
superset init
superset runserver
