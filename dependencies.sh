#!/usr/bin/env bash

sudo apt-get install build-essential libssl-dev libffi-dev python-dev python-pip libsasl2-dev libldap2-dev
pip install virtualenv
virualenv venv
source ./venv/bin/activate
cd src
pip install -r requirements.txt
