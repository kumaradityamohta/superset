* Make sure python version is 2.7

* Cloning https://bitbucket.org/kumaradityamohta/superset.git
```bash
mkdir -p /webapps/superset/
cd /webapps/superset/
git clone https://bitbucket.org/kumaradityamohta/superset.git
mv superset/ src/
```

* Initial setup and dependencies installation.
```bash
sudo apt-get install build-essential libssl-dev libffi-dev python-dev python-pip libsasl2-dev libldap2-dev
pip install virtualenv
virualenv venv
source ./venv/bin/activate
cd src
pip install -r requirements.txt
```

* Superset initialization.
```bash
fabmanager create-admin --app superset
superset db upgrade
superset load_examples
superset init
superset runserver
```

* Setup Gunicorn and Apache server.
